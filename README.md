# ✏ Enpitsu (鉛筆)

Your pencil in the terminal (for jotting down todos)

## Installation

This is not on `npm`. Clone this to your local machine:

    git clone https://gitlab.com/japorized/enpitsu.git && cd enpitsu

Instead of `npm`, a `Makefile` is more than what's needed.

    make install

**Note**: I highly recommend that you install enpitsu in the default destination,
because you do not need escalated privileges to do anything.

### Defaults

* Destination: `$HOME/.bin`
* Configuration: `$XDG_CONFIG_HOME`
* Data: `$XDG_DATA_HOME/tasks.json`

---

## About the tool

The goal is to create a cli personal task manager, which will hopefully serve as
a basis for an all-out personal task manager in GUI space later on.

### Plans (in no particular priority)

* Gantt-chart-like overview of tasks (perhaps on a webpage)

### Non-goals

* Task time tracker

## Current capabilities

* Add, remove, edit (basic), move tasks
* Set priority of tasks
* Star/Unstar tasks
* Search for/List tasks by tags
* Search for keywords in tasks
* List tasks by priority
* Show tasks whose date ranges over today, tomorrow, the day after, ...
* Minimal configuration (for now)

--- 

## Usage

Provided command: `enpitsu`, `enp`

```
enpitsu help
```

---

## Development

I make use of `entr` to automate copying files over to the distributed destination, so you need to have `entr` installed on your system for this to work. Otherwise, feel free to set up your own environment.

Run `make dev` to keep the tool running.

The only downside to this approach is when one ends up installing a new package from `npm`.
