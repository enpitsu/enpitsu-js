const chalk = require('chalk');

function toProperCase(str) {
    return str.replace(
        /\w\S*/g,
        function(txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        }
    );
}

function replaceCharAtIndex(str, index, replacement) {
  return str.substr(0, index) + replacement + str.substr(index + replacement.length);
}

function reverseObjectForIn(obj, f) {
  var arr = [];
  for ( var key in obj ) {
    arr.push(key);
  }
  for ( var i = arr.length - 1 ; i >= 0 ; i-- ) {
    f.call(obj, arr[i]);
  }
}

// This function should perform the following:
// if it is passed the string "1,4-8,10,12-14", it will return
// the array [1, 4, 5, 6, 7, 8, 10, 12, 13, 14].
// In other words, values are first comma-separated, and then
// the "-" is treated as an inclusive range of values.
function humanListToArray(input) {
  if ( input === '' || input == null ) {
    // empty string
    return [];
  } else if ( input.indexOf(',') == -1 ) {
    // no comma
    if ( input.indexOf('-') == -1 ) {
      // no range
      return [input];
    } else {
      // with range
      var array = [];
      for ( var se = input.split('-'), i = se[0] ; i <= se[1] ; i++ ) {
        array.push(i);
      }
      return array;
    }
  } else {
    // with comma
    var tmpArray = input.split(',');
    var array = [];
    for ( var i = 0 ; i < tmpArray.length ; i++ ) {
      if ( tmpArray[i].indexOf('-') == -1 ) {
        // no range
        array.push(tmpArray[i]);
      } else {
        // with range
        for ( var se = input.split('-'), j = se[0]; j <= se[1] ; j++ ) {
          array.push(j);
        }
      }
    }
    return array;
  }
}

// can use some improvement, perhaps more general
async function search(data, query, filter = ["name", "tags", "notes"], status = "active", fuzzy = false) {
  if ( query == null || query == "" ) {
    console.log(chalk`{red Please provide a search query}`);
    process.exit(1);
  }
  var cabinetDict = {};
  var cabinet = [];
  var tagOnly = (filter.length == 1 && filter.includes("tags"));
  if ( tagOnly ) {
    var searches = query.split(',');
  } else {
    var searches = [query];
  }
  for ( var key in data ) {
    if ( ! data[key].inUse )
      continue;

    var score = 0;
    for ( var i = 0 ; i < searches.length ; i++ ) {
      if ( tagOnly ) {
        for ( var j = 0 ; j < data[key].tags.length ; j++ ) {
          var isSubstring = ( searches[i].includes(data[key].tags[j]) || data[key].tags[j].includes(searches[i]) );
          if ( isSubstring ) {
            if ( searches[i] == data[key].tags[j] ) {
              score += 10;
            } else {
              if ( fuzzy )
                score++;
            }
          }
        }
      } else {
        // check title
        if ( data[key].id == 0 )  continue;
        if ( data[key].name.includes(searches[i]) ) {
          if ( data[key].name == searches[i] ) {
            score += 5;
          } else if ( fuzzy ) {
            score++;
          }
        }

        // check notes
        if ( data[key].notes.includes(searches[i]) ) {
          score += 2;
        }

        // check tags
        for ( var j = 0 ; j < data[key].tags.length ; j++ ) {
          var isSubstring = ( searches[i].includes(data[key].tags[j]) || data[key].tags[j].includes(searches[i]) );
          if ( isSubstring ) {
            if ( searches[i] == data[key].tags[j] ) {
              score += 5;
            } else {
              if ( fuzzy )
                score += 3;
            }
          }
        }
      }
    }

    // sort into cabinetDict by its score
    if ( score > 0 ) {
      if ( ! Array.isArray(cabinetDict[score]) ) cabinetDict[score] = new Array();
      data[key].score = score;
      cabinetDict[score].unshift(data[key]);
    }
  }

  for ( var key in cabinetDict ) {
    for ( var i = 0 ; i < cabinetDict[key].length ; i++ ) {
      cabinet.push(cabinetDict[key][i]);
    }
  }

  return cabinet;
}

module.exports = {
  toProperCase: toProperCase,
  replaceCharAtIndex: replaceCharAtIndex,
  reverseObjectForIn: reverseObjectForIn,
  humanListToArray: humanListToArray,
  search: search
}
