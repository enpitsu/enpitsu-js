const str = require('./strings.js');
const chalk = require('chalk');

function help(arg) {
  switch(arg) {
    case "a":
    case "add":
      console.log(chalk`    ${str.app}
Example:
  enp a 'follow up email'
  enp a 'take out trash' 
        
When in interactive mode:
  {magenta Date formats}   mmm dd, YYYY HH:MM:SS or mmm dd HH:MM:SS or mmm dd, YY HH:MM:SS
  {magenta Priority}       0, 1, 2, 3
  {magenta Tags}           comma-separated without spaces`)
      break;
    case 'r':
    case 'rm':
    case 'remove':
      console.log(`    ${str.app}
 Removes task entries.
 Command: remove, rm, r
 Example: enp r 1,3-5 (remove tasks 1, 3, 4, 5)`)
      break;
    case 'mv':
    case 'move':
      console.log(`    ${str.app}
 Move task entries.
 Command: move, mv
 Example: enp mv 6 4 (move task in slot 6 to 4)`)
      break;
    case 'star':
      console.log(`    ${str.app}
 Star a task.
 Command: star
 Example: enp star 1,3-5 (star tasks 1, 3, 4, 5)`)
      break;
    case 'unstar':
      console.log(`    ${str.app}
 Unstar a task.
 Command: unstar
 Example: enp unstar 1,3-5 (unstar tasks 1, 3, 4, 5)`)
      break;
    case 'c':
    case 'complete':
      console.log(`    ${str.app}
 Mark tasks as complete.
 Command: complete, c
 Example: enp c 1,3-5 (mark tasks 1, 3, 4, 5 as complete)`)
      break;
    case 'incomplete':
      console.log(`    ${str.app}
 Mark tasks as incomplete.
 Example: enp incomplete 1,3-5 (mark tasks 1, 3, 4, 5 as incomplete)`)
      break;
    case "organize":
      console.log(`    ${str.app}
 Cleans up your tasks by moving them up the list to the earliest empty
 slot they can take, and clears away all unused slots in the end.
 Example: enp organize`)
      break;
    case "s":
    case "show":
      showCommands();
      break;
    case "search":
      searchHelp();
      break;
    default:
      usage();
  }
}

function usage() {
  console.log(`
    ${str.app}

 Usage: enpitsu/enp <cmd>
 
 Commands:
   h, help           Show this help
   a, add            Add task
   r, rm, remove     Remove task
   e, edit           Edit task
   mv, move          Move task
   star              Star tasks (comma-separated, range)
   unstar            Unstar tasks (comma-separated, range)
   c, complete       Complete tasks (comma-separated)
   s, show           Show tasks
   search            Query the database`);
}

function showCommands() {
  console.log(` Usage: enpitsu/enp s <cmd> [id(s)]

 Commands:
   <empty>    Show all active tasks
   a, all     Show all tasks
   full       Show full info of tasks
   tags       Show by tags (no further arg to list by tags)
   priority   Show all by priority
   today      Show today's tasks
   3          Show tasks from today, tomorrow, and the day after
   days [n]   Show tasks from today to the (n-1)-th day
  `);
}

function searchHelp() {
  console.log(` Usage: enpitsu/enp search <query>
 Search through tasks for keyword (query)

 Example:
   enp search work
   enp search 'review session'
    `)
}

module.exports = {
  help: help,
  usage: usage
}
