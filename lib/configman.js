const os = require('os'),
      Store = require('data-store');

var confpath = os.homedir() + '/.config/enpitsu/config.json';
var store = new Store({ path: confpath });
var config = store.clone();

// Some taskLocation shortcuts
// Can be refined if there exists a better way to handle ENV variables in JS
if ( config.taskLocation.includes('$HOME/') ) {
  config.taskLocation = config.taskLocation.replace('$HOME', os.homedir());

} else if ( config.taskLocation.includes('~/') ) {
  config.taskLocation = config.taskLocation.replace('~', os.homedir());

} else if ( config.taskLocation.includes('$XDG_DATA_HOME') ) {
  if ( process.env.XDG_DATA_HOME == null || process.env.XDG_DATA_HOME == '' ) {
    config.taskLocation = config.taskLocation.replace('$XDG_DATA_HOME', os.homedir() + '/.local/share');

  } else {
    config.taskLocation = config.taskLocation.replace('$XDG_DATA_HOME', process.env.XDG_DATA_HOME);
  }

} else if ( config.taskLocation == null || config.taskLocation == '' ) {
  store.set('taskLocation', os.homedir() + '/.local/share/tasks.json');
  config.taskLocation = os.homedir() + '/.local/share/tasks.json'
}

// Setting up the editor
if ( config.editor == '' ) {
  config.editor = '/usr/bin/nano'
} else if ( config.editor == '$EDITOR' ) {
  config.editor = process.env.EDITOR
}

module.exports = {
  config: config
}
