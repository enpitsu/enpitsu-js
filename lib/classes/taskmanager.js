const fs = require('fs'),
      spawn = require('child_process').spawnSync,
      qoa = require('qoa'),
      str = require('../strings'),
      utils = require('../utils'),
      chalk = require('chalk'),
      Store = require('data-store'),
      Task = require('./task.js');
var show;

class TaskManager {
  constructor() {
    const config = require('../configman.js').config;
    this._taskfilecheck(config);
    this.config = config;
    this.store = new Store({ path: config.taskLocation });
    this.data = this._loadfile(this.store);
  }

  async addTask(name) {
    var entry = new Task();
    await entry.setName(name);
    console.log(`Creating task for ${entry.name}`);

    // Setting values
    await entry.setTargetDate('start');
    if ( entry.start !== 'none' )
      await entry.setTargetDate('end');
    await entry.setTags();
    await entry.setPriority();
    await entry.setNotes();

    entry.id = this.returnUnusedID();
    if ( this.data[0].total < entry.id ) {
      this.data[0].total++;
      this.data[entry.id] = entry;
    } else {
      this.data[entry.id] = Object.assign(entry);
    }
    this.data[0].active++;

    console.log(' Created entry:');
    show = require('../show');
    show.showTask(entry);
    this.saveData();
  }

  async editEntry(id) {
    if ( ! Number.isInteger(Number(id)) ) {
      console.log(chalk`{red An id should be an integer}`);
      process.exit(1);
    }
    if ( id == '0' ) {
      console.log(str.protectedID);
      process.exit(1);
    }
    if ( Object.keys(this.data).length == 1 ) {
      console.log('Nothing to change.');
      process.exit(0);
    }

    var target = this.data[id];
    show = require('../show');
    show.showFullTask(target);
    console.log();

    var val = await qoa.keypress({
      type: 'keypress',
      query: 'Which item do you wish to edit?',
      handle: 'response',
      menu: [
        'Title',
        'Start Date',
        'End Date',
        'Tags',
        'Priority',
        'Notes'
      ]
    });
    switch(val.response) {
      case 'Title':
        await this.data[id].setName();
        break;
      case 'Start Date':
        await this.data[id].setTargetDate('start');
        break;
      case 'End Date':
        await this.data[id].setTargetDate('end');
        break;
      case 'Tags':
        await this.data[id].editTags();
        break;
      case 'Priority':
        await this.data[id].setPriority();
        break;
      case 'Notes':
        await this.data[id].editNotes(this.config.editor);
        break;
    }
    
    this.saveData();
  }

  async moveEntry(target, dest) {
    if ( target == null || target == '' || dest == null || dest == '' ) {
      console.log(chalk`{red  Please specify target and destination (by id)}`);
      process.exit(1);
    } else if ( ! Number.isInteger(parseInt(target)) ) {
      console.log(chalk`{red  Target should be an integer}`);
      process.exit(1);
    } else if ( ! Number.isInteger(parseInt(dest)) ) {
      console.log(chalk`{red  Destination should be an integer}`);
      process.exit(1);
    }
  
    show = require('../show');
    if ( this.data[dest].inUse ) {
      console.log(`id ${dest} is in use:`);
      show.showTask(this.data[dest]);

      await qoa.confirm({
        type: 'confirm',
        query: 'Replace?',
        handle: 'confirmation',
        accept: 'y',
        deny: 'N'
      }).then(function(reply) {
        if ( ! reply.confirmation ) {
          console.log(' Moving process terminated.');
          process.exit(0);
        }
      });
    }
  
    this.data[dest].copyInfo(this.data[target]);
    this.data[target].remove();
  
    console.log(`Moved task ${target} to id ${dest}`);
    show.showTask(this.data[dest]);
    this.saveData();
  }

  async markTasks(arg, selection) {
    if ( arg !== 'complete' && arg !== 'incomplete' && arg !== 'star' && arg !== 'unstar') {
      return 0;
    }
  
    if ( Object.keys(this.data).length == 1 ) {
      console.log('Nothing to mark.');
      process.exit(0);
    }
  
    var selectionArray = utils.humanListToArray(selection);
    if ( selectionArray.length != 0 ) {
      for ( var i = 0 ; i < selectionArray.length ; i++ ) {
        if ( arg === 'complete' ) {
          this.data[selectionArray[i]].stat = 'completed';
          this.data[0].active--;
          this.data[0].completed++;
        } else if ( arg === 'incomplete' ) {
          this.data[selectionArray[i]].stat = 'active';
          this.data[0].active++;
          this.data[0].completed--;
        } else if ( arg === 'star' ) {
          this.data[selectionArray[i]].star = true;
          this.data[0].starred++;
        } else if ( arg === 'unstar' ) {
          this.data[selectionArray[i]].star = false;
          this.data[0].starred--;
        }
        show = require('../show');
        show.showTask(this.data[selectionArray[i]]);
      }
    } else {
      console.log('No id provided. Terminating process.');
      process.exit(0);
    }
    this.saveData();
  }

  async removeTask(input) {
    if ( input === '0' ) {
      console.log(str.protectedID);
      process.exit(1);
    }

    if ( Object.keys(this.data).length == 1 ) {
      console.log('Nothing to remove.');
      process.exit(0);
    }

    var entries = utils.humanListToArray(input);
    var targets = [];
    var _this = this;
  
    for ( var j = 0 ; j < entries.length; j++ ) {
      for ( var i = 1 ; i < Object.keys(_this.data).length ; i++ ) {
        if ( entries[j] == _this.data[i].id ) {
          if ( _this.data[i].inUse )
            targets.push(_this.data[i]);
        }
      }
    }
  
    if ( targets.length == 0 ) {
      console.log('Nothing to remove');
      process.exit(0);
    }
  
    show = require('../show');
    console.log("Removing:");
    for ( var i = 0 ; i < targets.length; i++ ) {
      show.showTask(targets[i], `[ ${targets[i].tags} ]`);
    }
  
    await qoa.confirm({
      type: 'confirm',
      query: chalk`{red Are you sure?}`,
      handle: 'confirmation',
      accept: 'y',
      deny: 'N'
    }).then(function(reply) {
      if ( reply.confirmation ) {

        for ( var i = 0 ; i < targets.length; i++ ) {
          for ( var j = 1 ; j < Object.keys(_this.data).length ; j++ ) {
            if ( targets[i].id == _this.data[j].id ) {
              if ( _this.data[j].stat === 'active' ) {
                _this.data[0].active--;
              } else if ( _this.data[j].stat === 'completed' ) {
                _this.data[0].completed--;
              }
              if ( _this.data[j].star ) {
                _this.data[0].starred--;
              }
              _this.data[j].remove();
            }
          }
        }

        console.log(`Successfully removed!`)
      } else {
        console.log('No action taken');
        process.exit(0);
      }
    });

    _this.saveData();
  }

  _loadfile(store) {
    var data = store.clone();

    for ( var key in data ) {
      if ( key == 0 ) continue;

      var task = new Task();
      task.copyInfo(data[key], true);
      data[key] = task;
    }

    return data;
  }
  
  _taskfilecheck(config) {
    if ( !fs.existsSync(config.taskLocation) ) {
      console.log('tasks.json not found. Creating file at configured location.');
      var beginFile = {
        0: {
          id: 0,
          active: 0,
          completed: 0,
          starred: 0,
          total: 0
        }
      };

      // check if directory already exists
      var pathArr = config.taskLocation.split('/'),
          dirPath = '';
      for ( var i = 0 ; i < pathArr.length - 1 ; i++ ) {
        dirPath += pathArr[i] + '/';
      }
      if ( ! fs.existsSync(dirPath) ) {
        fs.mkdirSync(dirPath, { recursive: true });
      }
    }
  }

  returnUnusedID() {
    if ( Object.keys(this.data).length == 1 ) {
      return this.data[0].total + 1;
    } else if ( this.data[this.data[0].total].inUse ) {
      return this.data[0].total + 1;
    }
    var newestNotUsed;
    for ( var i = this.data[0].total ; i > 0 ; i-- ) {
      if ( !this.data[i].inUse )
        newestNotUsed = i;
      else {
        break;
      }
    }
    return newestNotUsed;
  }

  organize() {
    // organize existing tasks to top
    for (let i in this.data) {
      if (i == 0) continue;
      if ( this.data[i].inUse ) {
        for ( let j = 1 ; j < i ; j++ ) {
          if ( ! this.data[j].inUse ) {
             this.moveEntry(i, j);
             break;
          }
        }
      }
    }

    // remove all unused entries
    for ( let i in this.data ) {
      if ( i == 0 ) continue;
      if ( ! this.data[i].inUse ) {
        delete this.data[i];
      }
    }

    this.data[0].total = Object.keys(this.data).length - 1;
    this.saveData();
  }

  saveData() {
    this.store.data = this.data;
    this.store.save();
  }
}

module.exports = TaskManager;
